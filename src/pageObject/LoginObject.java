package pageObject;

import org.openqa.selenium.By;

public class LoginObject {
	public static By txtUsername = By.name("username");
	public static By txtPassword = By.name("password");
	//
	public static By btnLogin = By.xpath("//input[@value='Login']");
	//
	public static By verifytext =By.xpath("//td[@class='login-info-left']");
	
}
