package pageObject;

import org.openqa.selenium.By;

public class RoadMapObject {
	public static By btnDelete = By.xpath("//input[@value='Delete Issues']");
	//
	public static By Bugnote = By.name("bugnote_text");
	//click button close issue
	public static By btnClose = By.xpath("//input[@value='Close Issues']");
}
