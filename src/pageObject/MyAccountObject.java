package pageObject;

import org.openqa.selenium.By;


public class MyAccountObject {
	//
	public static By txtLinkAcc = By.xpath("//a[contains(.,'My Account')]");
	public static By txtReporter = By.xpath("//form/table/tbody/tr[7]/td[2]");
	public static By txtDev = By.xpath("//form/table/tbody/tr[8]/td[2]");
	public static By txtFormTitle = By.xpath("//form/table/tbody/tr[1]/td[1]");
	public static By txtUsername = By.xpath("//form/table/tbody/tr[2]/td[1]");
	public static By txtPassword = By.xpath("//form/table/tbody/tr[3]/td[1]");
	public static By txtConfirmPassword = By.xpath("//form/table/tbody/tr[4]/td[1]");
	public static By txtEmail = By.xpath("//form/table/tbody/tr[5]/td[1]");
	public static By txtRealName =By.xpath("//form/table/tbody/tr[6]/td[1]");
	public static By txtAccessLevel = By.xpath("//form/table/tbody/tr[7]/td[1]");
	public static By txtProjectAccess = By.xpath("//form/table/tbody/tr[8]/td[1]");
	public static By txtAssignedProjects = By.xpath("//form/table/tbody/tr[9]/td[1]");
	

}
