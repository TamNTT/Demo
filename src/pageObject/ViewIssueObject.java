package pageObject;

import org.openqa.selenium.By;

public class ViewIssueObject {

	public static By txtLinkView = By.xpath("//a[contains(.,'View Issues')]");
	public static By txtMessage = By.className("floatleft");
	public static By cbAllbugs = By.name("all_bugs");
	public static By cbDelete = By.xpath("//select[@name='action']");
	public static By btnOK = By.xpath("//input[contains(@value,'OK')]");
	public static By VerifyDelete = By.xpath("//span[contains(.,'Viewing Issues (0 - 0 / 0) ')]");
	public static By txtAction= By.name("action");
	public static By btnFilter = By.xpath("//a[@id='show_status_filter']");
	public static By slClose = By.xpath("html/body/div[3]/form/table/tbody/tr[4]/td[1]/select");
	public static By txtText = By.className("issue-status");
	public static By txtResolve = By.xpath("//td[contains(.,'resolved (test)')]");
	public static By txtPriority = By.xpath("//input[@value='Update Priority']");
	public static By txtImg = By.xpath("//table/tbody/tr[4]/td[2]/a/img");
	public static By txtTextPri = By.xpath("html/body/table[3]/tbody/tr[7]/td[2]");
	}
