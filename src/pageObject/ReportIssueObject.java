package pageObject;

import org.openqa.selenium.By;

public class ReportIssueObject {
	public static By txtlink = By.xpath("//a[@href='/mantis/bug_report_page.php']");
	public static By clxCategory = By.name("category_id");
	public static By clxRepodu = By.name("reproducibility");
	public static By clxSenkey = By.name("severity");
	public static By clxprio = By.name("priority");
	public static By txtPlat = By.name("platform");
	public static By txtOs = By.id("os");
	public static By txtOsversion = By.id("os_build");
	public static By txtAssign = By.name("handler_id");
	public static By txtSummary = By.name("summary");
	public static By txtDes = By.name("description");
	public static By txtStep = By.name("steps_to_reproduce");
	public static By txtInfor = By.name("additional_info");
	public static By chkStatus = By.name("view_state");
	public static By chkStay = By.name("report_stay");
	public static By btnButton = By.className("button");

}
