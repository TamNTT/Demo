package tcLogin;

import java.sql.Connection;
import java.sql.Statement;
import org.testng.annotations.*;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CheckSalary {
	// Config information
	static Connection con = null;
	private static Statement stmt;
	public static String DB_URL = "jdbc:mysql://localhost:3306/testdb";
	public static String DB_USER = "root";
	public static String DB_PASSWORD = "";
	// SQL queries
	String querySelectSalary = "SELECT SALARY FROM CUSTOMERS WHERE ID = 6";

	@BeforeTest
	public void setUp() throws Exception {
		try {
			// Make the database connection
			String dbClass = "com.mysql.jdbc.Driver";
			Class.forName(dbClass).newInstance();

			// Get connection to DB
			Connection con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
			// Statement object to send the SQL statement to the Database
			stmt = con.createStatement();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(enabled = true)
	public void querySelectSalary() throws SQLException {
		// try {
		// Get the contents of table from DB
		ResultSet res = stmt.executeQuery(querySelectSalary);

		// Print the all result
		while (res.next()) {
			String cusSalary = res.getString(1);

			if (cusSalary.equals("8500.00")) {
				System.out.println("pass");
			} else {
				System.out.println("false");
			}
		}

		// } catch (Exception e) {
		// e.printStackTrace();
	}
	// }

	@AfterTest
	public void tearDown() throws Exception {
		// Close DB connection
		if (con != null) {
			con.close();
		}

	}
}
