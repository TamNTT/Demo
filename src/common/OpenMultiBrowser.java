package common;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class OpenMultiBrowser {
		
	public static void multi_browser(String browser) throws Exception{
		//Firefox
		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
			Utils.driver = new FirefoxDriver();
			Utils.driver.manage().window().maximize();
			
		//Chrome	
		} else if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
			Utils.driver = new ChromeDriver();	
			Utils.driver.manage().window().maximize();
	
		} else if (browser.equalsIgnoreCase("ie")) {
			 System.setProperty("webdriver.ie.driver", "IEDriverServer.exe");
			  DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
			  caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			  caps.setCapability("nativeEvents",false);
			  Utils.driver = new InternetExplorerDriver(caps);			  			  
			  Utils.driver.manage().window().maximize();
			  
		} else {
			throw new IllegalArgumentException("The Browser Type is undefined");
		}			
		Utils.driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);				
	}	
}