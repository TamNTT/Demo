package common;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import pageObject.LoginObject;
import pageObject.ReportIssueObject;
import pageObject.RoadMapObject;
import pageObject.ViewIssueObject;

public class Functions {
	public static WebDriver driver;
	public static String ExcelName = "testData.xlsx";
	public static String SheetLogin = "login";
	public static String SheetReportissue = "reportissue";

	@Test
	public static void commonLogin() throws Exception {
		// Load the URL
		Utils.driver.get(Utils.URL);
		XSSFSheet ExcelDataLogIn = ExcelCommon_POI.setExcelFile(ExcelName, SheetLogin);
		String Username = ExcelCommon_POI.getCellData(1, 1, ExcelDataLogIn);
		String Password = ExcelCommon_POI.getCellData(1, 2, ExcelDataLogIn);
		// Enter the user name
		Utils.driver.findElement(LoginObject.txtUsername).sendKeys(Username);
		// Enter the password
		Utils.driver.findElement(LoginObject.txtPassword).sendKeys(Password);
		// click button login
		Utils.driver.findElement(LoginObject.btnLogin).click();
		Thread.sleep(5000);
		// verify login success
		String VerifyText = Utils.driver.findElement(LoginObject.verifytext).getText();
		String ExpectedText = "Logged in as: test (sasa - developer)";
		Assert.assertEquals(ExpectedText, VerifyText);

	}

	@Test
	public static void commonLogout() throws Exception {
		Utils.driver.findElement(By.xpath("//a[@href='/mantis/logout_page.php']")).click();
		Thread.sleep(5000);
	}

	@Test
	public static void commonCreateIssue() throws Exception {
		// report issue page
		Utils.driver.findElement(ReportIssueObject.txtlink).click();
		Thread.sleep(5000);
		XSSFSheet ExcelDataReportissue = ExcelCommon_POI.setExcelFile(ExcelName, SheetReportissue);
	
		String bxCategory = ExcelCommon_POI.getCellData(1, 1, ExcelDataReportissue);
		String bxRepro = ExcelCommon_POI.getCellData(1, 2, ExcelDataReportissue);		
		String bxSeverity = ExcelCommon_POI.getCellData(1, 3, ExcelDataReportissue);		
		String bxPriority = ExcelCommon_POI.getCellData(1, 4, ExcelDataReportissue);		
		String txtPlatform = ExcelCommon_POI.getCellData(1, 5, ExcelDataReportissue);		
		String txtOs = ExcelCommon_POI.getCellData(1, 6, ExcelDataReportissue);		
		String txtOsVer = ExcelCommon_POI.getCellData(1, 7, ExcelDataReportissue);
		String bxAssign = ExcelCommon_POI.getCellData(1, 8, ExcelDataReportissue);
		String txtSumary = ExcelCommon_POI.getCellData(1, 9, ExcelDataReportissue);
		String txtDescription = ExcelCommon_POI.getCellData(1, 10, ExcelDataReportissue);
		String txtStep = ExcelCommon_POI.getCellData(1, 11, ExcelDataReportissue);
		String txtAddInfor = ExcelCommon_POI.getCellData(1, 12, ExcelDataReportissue); 
		Utils.driver.findElement(ReportIssueObject.clxCategory).sendKeys(bxCategory);
		// Select Reproducibility
		Utils.driver.findElement(ReportIssueObject.clxRepodu).sendKeys(bxRepro);
		// Select Severity
		Utils.driver.findElement(ReportIssueObject.clxSenkey).sendKeys(bxSeverity);
		// Select Priority
		Utils.driver.findElement(ReportIssueObject.clxprio).sendKeys(bxPriority);
		// Enter Platform
		Utils.driver.findElement(ReportIssueObject.txtPlat).sendKeys(txtPlatform);
		// Enter OS
		Utils.driver.findElement(ReportIssueObject.txtOs).sendKeys(txtOs);
		// Enter OS Version
		Utils.driver.findElement(ReportIssueObject.txtOsversion).sendKeys(txtOsVer);
		// Select Assign To
		Utils.driver.findElement(ReportIssueObject.txtAssign).sendKeys(bxAssign);
		// Enter Summary
		Utils.driver.findElement(ReportIssueObject.txtSummary).sendKeys(txtSumary);
		// Enter Description
		Utils.driver.findElement(ReportIssueObject.txtDes).sendKeys(txtDescription);
		// Enter Steps To Reproduce
		Utils.driver.findElement(ReportIssueObject.clxRepodu).sendKeys(txtStep);
		// Enter Additional Information
		Utils.driver.findElement(ReportIssueObject.txtInfor).sendKeys(txtAddInfor);
		// Click checkbox View Status
		Utils.driver.findElement(ReportIssueObject.chkStatus).click();
		// Click checkbox Report Stay
		Utils.driver.findElement(ReportIssueObject.chkStay).click();
		// click button login
		Utils.driver.findElement(ReportIssueObject.btnButton).click();
		Thread.sleep(7000);
		// verifytext
		boolean VerifyText1 = Utils.driver.getPageSource().contains("Operation successful.");
		Assert.assertEquals(true, VerifyText1);

	}

	@Test
	public static void commonSearchIssue() throws Exception {
		// Click on view issue page
		Utils.driver.findElement(ViewIssueObject.txtLinkView).click();
		Thread.sleep(5000);
		// search issue
		Utils.driver.findElement(By.name("search")).clear();
		Utils.driver.findElement(By.name("search")).sendKeys("A1-0917466329");
		// Click button apply filter
		Utils.driver.findElement(By.name("filter")).click();
		Thread.sleep(5000);
		// verify after search
		/*
		 * String VerifyText = Utils.driver.findElement(By.xpath(
		 * "//span[contains(.,'Viewing Issues (1 - 1 / 1) ')]")) .getText();
		 * String ExpectedText = "Viewing Issues (1 - 1 / 1)";
		 * Assert.assertEquals(ExpectedText, VerifyText);
		 */
	}

	@Test
	public static void commonDelete() throws Exception{		
		
		Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
		// select action
		Utils.driver.findElement(ViewIssueObject.cbDelete).sendKeys("Delete");
		// Click button OK
		Utils.driver.findElement(ViewIssueObject.btnOK).click();
		// Click button delete issue
		Utils.driver.findElement(RoadMapObject.btnDelete).click();
		//verify delete
		Utils.driver.findElement(ViewIssueObject.VerifyDelete).click();
		Thread.sleep(3000);
		// Verify delete
		String VerifyDeleteSucc = Utils.driver.findElement(ViewIssueObject.txtMessage).getText();
		String ExpectedDeleteSucc = "Viewing Issues (0 - 0 / 0)";
		Assert.assertEquals(ExpectedDeleteSucc, VerifyDeleteSucc);
		
	}
	@AfterTest
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
