package testcase;

import java.util.Random;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.ExcelCommon_POI;
import common.Utils;

public class CheckSignupNewAccount {
	private int randomInt;
	public static String ExcelName = "testData.xlsx";
	public static String SheetVerifyText = "VerifyText.xlxs";

	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();

	}

	@Test(enabled = true, priority = 1)
	public void AccessLevelreporter() throws Exception {
		//
		Utils.driver.get(Utils.URL);
		XSSFSheet ExcelDataVerifySignup = ExcelCommon_POI.setExcelFile(ExcelName, SheetVerifyText);
		// click on link signup
		Utils.driver.findElement(By.xpath("//a[@href='signup_page.php']")).click();
		Thread.sleep(5000);
		// random
		Random rd = new Random();
		for (int idx = 1000; idx <= 100000; ++idx) {
			randomInt = rd.nextInt(100000);
		}
		// Enter username
		Utils.driver.findElement(By.name("username")).sendKeys("TamNTT" + randomInt);
		// Enter Password
		Utils.driver.findElement(By.name("email")).sendKeys("tam@gmail.com");
		// verify message
		/*
		 * String VerifySignup =
		 * Utils.driver.findElement(By.xpath("//table/tbody/tr[4]/td")).getText(
		 * ); Thread.sleep(5000); System.out.println(VerifySignup); String
		 * ExpectedVerifySignup = ExcelCommon_POI.getCellData(1, 1,
		 * ExcelDataVerifySignup); Assert.assertEquals(VerifySignup,
		 * ExpectedVerifySignup);
		 */
		// Click submit
		// Utils.driver.findElement(By.xpath("//input[@value='Signup']")).click();
		// Thread.sleep(5000);

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();
	}
}
