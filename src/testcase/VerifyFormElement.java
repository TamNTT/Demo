package testcase;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.Functions;
import common.Utils;
import pageObject.MyAccountObject;

public class VerifyFormElement {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
		// click linkAcc
		Utils.driver.findElement(MyAccountObject.txtLinkAcc).click();
		Thread.sleep(3000);

	}

	@Test
	public void VerifyTitle() throws Exception {
		boolean FormTitle = Utils.driver.findElement(MyAccountObject.txtFormTitle).isDisplayed();
		boolean Username = Utils.driver.findElement(MyAccountObject.txtUsername).isDisplayed();
		boolean Password = Utils.driver.findElement(MyAccountObject.txtPassword).isDisplayed();
		boolean ConfirmPassword = Utils.driver.findElement(MyAccountObject.txtConfirmPassword).isDisplayed();
		boolean Email = Utils.driver.findElement(MyAccountObject.txtEmail).isDisplayed();
		boolean RealName = Utils.driver.findElement(MyAccountObject.txtRealName).isDisplayed();
		boolean AccessLevel = Utils.driver.findElement(MyAccountObject.txtAccessLevel).isDisplayed();
		boolean ProjectAccess = Utils.driver.findElement(MyAccountObject.txtProjectAccess).isDisplayed();
		boolean AssignedProjects = Utils.driver.findElement(MyAccountObject.txtAssignedProjects).isDisplayed();
		boolean FinalResoun;
		if (FormTitle && Username && Password && ConfirmPassword && Email && RealName && RealName && AccessLevel
				&& ProjectAccess && AssignedProjects) {
			FinalResoun = true;
			System.out.print("Testcase passed");
		} else {
			FinalResoun = false;
			System.out.print("Testcase failed");
		}

		Assert.assertEquals(true, FinalResoun);

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
