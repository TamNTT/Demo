package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.CloseIssueObject;
import pageObject.ResolveIssueObject;
import pageObject.ViewIssueObject;

public class ChangeStatusToResolve {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
	}

	@Test
	public void VerifyLogo() throws Exception {
		// search issue
		Functions.commonSearchIssue();
		if (Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// create report issue
			Functions.commonCreateIssue();
			// search issue
			Functions.commonSearchIssue();
			// select bug
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Resolve");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			Utils.driver.findElement(CloseIssueObject.txtBugnote).sendKeys("Tam like bugs very much");
			Utils.driver.findElement(ResolveIssueObject.txtResolveIssue).click();
			// verify text
			String VerifySumary = Utils.driver.findElement(ViewIssueObject.txtResolve).getText();
			String ExpectedSumary = "resolved (test)";
			Assert.assertEquals(ExpectedSumary, VerifySumary);
			// Delete Issue after Resolved
			Functions.commonDelete();

		} else {
			// select bug
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Resolve");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			Utils.driver.findElement(CloseIssueObject.txtBugnote).sendKeys("Tam like bugs very much");
			Utils.driver.findElement(ResolveIssueObject.txtResolveIssue).click();
			// verify text
			String VerifySumary = Utils.driver.findElement(ViewIssueObject.txtResolve).getText();
			String ExpectedSumary = "resolved (test)";
			Assert.assertEquals(ExpectedSumary, VerifySumary);
			// Delete Issue after Resolved
			Functions.commonDelete();

		}
	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}
}
