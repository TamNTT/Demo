package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.OpenMultiBrowser;
import common.Utils;

public class TestCaseLevel2 {
	//@Parameters("browser")

	@BeforeMethod
	public void before() throws Exception {
	//	OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		// System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		// Utils.driver = new FirefoxDriver();
		// Utils.driver.manage().timeouts().implicitlyWait(15,
		// TimeUnit.SECONDS);
	}

	@Test
	public void CheckWaitTime() throws Exception {
		Utils.driver.get(Utils.URL1);
		// Thread.sleep(5000);
		WebDriverWait wait = new WebDriverWait(Utils.driver, 30);
		WebElement element = wait
				.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='proceed']")));
		element.click();
	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();
	}

}
