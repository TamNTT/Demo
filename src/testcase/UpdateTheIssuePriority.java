package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.Functions;
import common.Utils;
import pageObject.ViewIssueObject;

public class UpdateTheIssuePriority {
	@BeforeMethod
	public void before() throws Exception {
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
	}

	@Test
	public void IssuePriority() throws Exception {
		// search issue
		Functions.commonSearchIssue();
		if (Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// create report issue
			Functions.commonCreateIssue();
			// search issue
			Functions.commonSearchIssue();
			// select issue
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Update Priority");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			// choose issue priority
			Utils.driver.findElement(By.name("priority")).sendKeys("high");
			Thread.sleep(3000);
			// click button update priority
			Utils.driver.findElement(ViewIssueObject.txtPriority).click();
			// Search issue
			Functions.commonSearchIssue();
			Utils.driver.findElement(ViewIssueObject.txtImg).click();
			// click button update priority
			Utils.driver.findElement(By.xpath("//table/tbody/tr[18]/td/input")).click();
			// verify text
			String VerifyPriority = Utils.driver.findElement(ViewIssueObject.txtTextPri).getText();
			String ExpectedPriority = "high";
			Assert.assertEquals(ExpectedPriority, VerifyPriority);
			// search issue
			Functions.commonSearchIssue();
			// Delete issue
			Functions.commonDelete();
		} else {
			// select issue
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Update Priority");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			// choose issue priority
			Utils.driver.findElement(By.name("priority")).sendKeys("high");
			Thread.sleep(3000);
			// click button update priority
			Utils.driver.findElement(ViewIssueObject.txtPriority).click();
			// Search issue
			Functions.commonSearchIssue();
			Utils.driver.findElement(ViewIssueObject.txtImg).click();
			// click button update priority
			Utils.driver.findElement(By.xpath("//table/tbody/tr[18]/td/input")).click();
			// verify text
			String VerifyPriority = Utils.driver.findElement(ViewIssueObject.txtTextPri).getText();
			String ExpectedPriority = "high";
			Assert.assertEquals(ExpectedPriority, VerifyPriority);
			// search issue
			Functions.commonSearchIssue();
			// Delete issue
			Functions.commonDelete();

		}
	}

	@AfterMethod
	public void aftertest() throws Exception {
		// logout
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
