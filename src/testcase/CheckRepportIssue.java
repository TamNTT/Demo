package testcase;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.RoadMapObject;
import pageObject.ViewIssueObject;

public class CheckRepportIssue {

	//@Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		//OpenMultiBrowser.multi_browser(browser);
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
	}

	@Test
	public void VerifyLogo() throws Exception {
		//search issue
		Functions.commonSearchIssue();
		if(Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)"))
		{
		// Create report issue
		Functions.commonCreateIssue();
		// Search issue created
		Functions.commonSearchIssue();
		//Delete issue
		Functions.commonDelete();		
		}
		else
		{
		//Delete issue
		Functions.commonDelete();
		// Create report issue
		Functions.commonCreateIssue();
		// Search issue created
		Functions.commonSearchIssue();
		//Delete issue
		Functions.commonDelete();
		}

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();

		// close page
		Utils.driver.quit();
	}

}
