package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.MyviewObject;

public class CheckFooterLogoSize {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
	}

	@Test
	public void VerifyLogo() throws Exception {

		// login success
		Functions.commonLogin();
		/*
		 * WebDriverWait wait = new WebDriverWait(Utils.driver, 15); WebElement
		 * element =
		 * wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
		 * "//button[@id='proceed']"))); element.click();
		 */

		// verify width logo
		String actualWidth = Utils.driver.findElement(MyviewObject.txtWeight).getAttribute("width");
		String actualHeight = Utils.driver.findElement(MyviewObject.txtHeight).getAttribute("height");
		//boolean FinalResoun;
		//System.out.println(actualWidth);
		//System.out.println(actualHeight);
		if ((actualWidth.equals("145")) && actualHeight.equals("50")) {
				System.out.print("logo size is correct");

		} else {
			
			System.out.println("Logo size is incorrect."+"Height is" + actualHeight+"Weight is" + actualWidth +"." );
		}
		Assert.assertEquals(actualWidth, "145");
		Assert.assertEquals(actualHeight, "50");

		// logout page
		Functions.commonLogout();

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();

	}

}
