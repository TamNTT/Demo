package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.Functions;
import common.Utils;

public class CheckLostPassword {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// open page
		Utils.driver.get(Utils.URL);
	}

	@Test
	public void VerifyLogo() throws Exception {
		Utils.driver.findElement(By.xpath("//a[@href='lost_pwd_page.php']")).click();
		// Enter Username and pass
		Utils.driver.findElement(By.name("username")).sendKeys("TamTest");
		Utils.driver.findElement(By.name("email")).sendKeys("tamtest@gmail.com");
		Utils.driver.findElement(By.xpath("//input[@type='submit']")).click();
		Thread.sleep(5000);
		String issueStatus = Utils.driver.findElement(By.xpath("html/body/div[2]/table/tbody/tr[2]/td")).getText();
		System.out.println(issueStatus);
		// Click on proceed and back to login page
		Utils.driver.findElement(By.xpath("//a[contains(.,'Proceed')]")).click();

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();
	}

}
