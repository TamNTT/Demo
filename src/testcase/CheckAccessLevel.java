package testcase;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.MyAccountObject;

public class CheckAccessLevel {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// penMultiBrowser.multi_browser(browser);

		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
		// click linkAcc
		Utils.driver.findElement(MyAccountObject.txtLinkAcc).click();
		Thread.sleep(3000);
	}

	@Test(enabled = true, priority = 1)
	public void AccessLevelreporter() throws Exception {

		// Click link My Account
		String VerifyReporter = Utils.driver.findElement(MyAccountObject.txtReporter).getText();
		String ExpectReporter = "reporter";
		// System.out.println(VerifyAccess);
		Assert.assertEquals(VerifyReporter, ExpectReporter);
		String VerifyDev = Utils.driver.findElement(MyAccountObject.txtDev).getText();
		// System.out.println(VerifyProject);
		String ExpectedDev = "developer";
		Assert.assertEquals(VerifyDev, ExpectedDev);
		

	}

	@Test(enabled = true, priority = 2)
	public void ProjectAccesslevel() throws Exception {

		String VerifyDev = Utils.driver.findElement(MyAccountObject.txtDev).getText();
		// System.out.println(VerifyProject);
		String ExpectedDev = "developer";
		Assert.assertEquals(VerifyDev, ExpectedDev);

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
