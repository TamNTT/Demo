package testcase;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.ViewIssueObject;

public class CheckDeleteIssue {
	//@Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		//OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
	}

	@Test
	public void DeleteIssue() throws Exception {
		// login success
		Functions.commonLogin();
		// search issue
		Functions.commonSearchIssue();
		if (Utils.driver.getPageSource().contains("Viewing Issues (1 - 1 / 1)")) {
			// Print created
			System.out.println("Đã tồn tại");
			// select issue and delete issue
			Functions.commonDelete();
			// search issue vua xoa
			Functions.commonSearchIssue();
			// verify deleted
			String VerifyText = Utils.driver.findElement(ViewIssueObject.VerifyDelete).getText();
			String ExpectedText = "Viewing Issues (0 - 0 / 0)";
			Assert.assertEquals(ExpectedText, VerifyText);
		} else {
			// create issue
			Functions.commonCreateIssue();
			// search issue
			Functions.commonSearchIssue();
			// select delete issue
			Functions.commonDelete();
			// search issue vua xoa
			Functions.commonSearchIssue();
			// verify deleted
			String VerifyText = Utils.driver.findElement(ViewIssueObject.VerifyDelete).getText();
			String ExpectedText = "Viewing Issues (0 - 0 / 0)";
			Assert.assertEquals(ExpectedText, VerifyText);
		}

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
