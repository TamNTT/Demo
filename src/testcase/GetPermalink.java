package testcase;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.Functions;
import common.Utils;
import pageObject.ViewIssueObject;

public class GetPermalink {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
	}

	@Test
	public void VerifyLogo() throws Exception {
		Utils.driver.findElement(ViewIssueObject.txtLinkView);
		//Utils.driver.findElement(By.xpath("//a[@class=' firepath-matching-node']")).click();
		WebElement element = Utils.driver.findElement(By.linkText("Create Permalink"));
		element.click();
		//check link on new brower
		String oldTab = Utils.driver.getWindowHandle();
	    Utils.driver.findElement(By.linkText("Twitter Advertising Blog")).click();
	    ArrayList<String> newTab = new ArrayList<String>(Utils.driver.getWindowHandles());
	    newTab.remove(oldTab);
	    // change focus to new tab
	    Utils.driver.switchTo().window(newTab.get(0));
	    assertAdvertisingBlog();
	 // Do what you want here, you are in the new tab

	    Utils.driver.close();
	    // change focus back to old tab
	    Utils.driver.switchTo().window(oldTab);
	    assertStartAdvertising();
	}
	private void assertStartAdvertising() {
	    assertEquals("Start Advertising | Twitter for Business", Utils.driver.getTitle());
	}

	private void assertAdvertisingBlog() {
	    assertEquals("Twitter Advertising",Utils.driver.getTitle());
	}

	

	@AfterMethod
	public void tearDown() throws Exception {
		// logout
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
