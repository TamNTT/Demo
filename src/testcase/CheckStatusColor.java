package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;

public class CheckStatusColor {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login page
		Functions.commonLogin();

	}

	@Test(enabled = true, priority = 1)
	public void CheckNewColor() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#fcbdbd')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#fcbdbd", NewColor);

	}

	@Test(enabled = true, priority = 2)
	public void CheckFeedbackColor() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#e3b7eb')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#e3b7eb", NewColor);

	}

	@Test(enabled = true, priority = 3)
	public void CheckacKnowLedged() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#ffcd85')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#ffcd85", NewColor);

	}

	@Test(enabled = true, priority = 4)
	public void CheckConfirmed() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#fff494')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#fff494", NewColor);

	}

	@Test(enabled = true, priority = 5)
	public void CheckAssigned() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#c2dfff')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#c2dfff", NewColor);

	}

	@Test(enabled = true, priority = 6)
	public void CheckResolved() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#d2f5b0')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#d2f5b0", NewColor);

	}

	@Test(enabled = true, priority = 7)
	public void CheckClosed() throws Exception {
		String NewColor = Utils.driver.findElement(By.xpath("//td[contains(@bgcolor,'#c9ccc4')]"))
				.getAttribute("bgcolor");
		// System.out.println(NewColor);
		Assert.assertEquals("#c9ccc4", NewColor);

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();
	}

}
