package testcase;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.ViewIssueObject;

public class SearchForTheIssue {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
	}

	@Test
	public void SearchIssue() throws Exception {
		// login success
		Functions.commonLogin();
		// Click on view issue page
		Functions.commonSearchIssue();
		if (Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// create issue
			Functions.commonCreateIssue();
			// search issue
			Functions.commonSearchIssue();
			// verify text
			String VerifyText = Utils.driver.findElement(By.xpath("//span[contains(.,'Viewing Issues (1 - 1 / 1) ')]"))
					.getText();
			String ExpectedText = "Viewing Issues (1 - 1 / 1)";
			Assert.assertEquals(ExpectedText, VerifyText);
		} else {
			System.out.print("ID da ton tai");

		}

		// logout page
		Functions.commonLogout();
	}

	@AfterMethod
	public void tearDown() throws Exception {
		// close page
		Utils.driver.quit();
	}
}
