package testcase;

import org.openqa.selenium.By;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.Functions;
import common.OpenMultiBrowser;
import common.Utils;
import pageObject.CloseIssueObject;
import pageObject.RoadMapObject;
import pageObject.ViewIssueObject;

public class ChangeStatusToClose {
	// @Parameters("browser")
	@BeforeMethod
	public void before() throws Exception {
		// OpenMultiBrowser.multi_browser(browser);
		// open brower firefox
		System.setProperty("webdriver.gecko.driver", Utils.Geckodriver);
		Utils.driver = new FirefoxDriver();
		// login success
		Functions.commonLogin();
	}

	@Test
	public void ChangeStatus() throws Exception {
		// Search issue
		Functions.commonSearchIssue();
		if (Utils.driver.getPageSource().contains("Viewing Issues (0 - 0 / 0)")) {
			// create issue
			Functions.commonCreateIssue();
			// select bug
			Functions.commonSearchIssue();
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// Utils.driver.findElement(ViewIssueObject.txtAction).sendKeys("Close");
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Close");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			Utils.driver.findElement(CloseIssueObject.txtBugnote).sendKeys("tam tests everything");
			Utils.driver.findElement(CloseIssueObject.btnCloseIssue).click();
			// verify after close
			Utils.driver.findElement(ViewIssueObject.txtLinkView).click();
			Utils.driver.findElement(ViewIssueObject.btnFilter).click();
			Thread.sleep(5000);
			Select dropdown1 = new Select(Utils.driver.findElement(ViewIssueObject.slClose));
			dropdown1.selectByVisibleText("closed");
			Thread.sleep(5000);
			// search issue
			Utils.driver.findElement(By.name("filter")).click();
			Functions.commonSearchIssue();
			// verify text
			String VerifyText = Utils.driver.findElement(ViewIssueObject.txtText).getText();
			String ExpectedText = "closed";
			Assert.assertEquals(ExpectedText, VerifyText);
			// delete issue after close
			Functions.commonDelete();
		} else {
			// select bug
			Utils.driver.findElement(ViewIssueObject.cbAllbugs).click();
			// select close action
			Select dropdown = new Select(Utils.driver.findElement(ViewIssueObject.txtAction));
			dropdown.selectByVisibleText("Close");
			Utils.driver.findElement(ViewIssueObject.btnOK).click();
			Utils.driver.findElement(CloseIssueObject.txtBugnote).sendKeys("tam tests everything");
			Utils.driver.findElement(CloseIssueObject.btnCloseIssue).click();
			// verify after close
			Utils.driver.findElement(ViewIssueObject.txtLinkView).click();
			Utils.driver.findElement(ViewIssueObject.btnFilter).click();
			Thread.sleep(5000);
			Select dropdown1 = new Select(Utils.driver.findElement(ViewIssueObject.slClose));
			dropdown1.selectByVisibleText("closed");
			Thread.sleep(5000);
			// search issue
			Utils.driver.findElement(By.name("filter")).click();
			Functions.commonSearchIssue();
			// verify text
			String VerifyText = Utils.driver.findElement(ViewIssueObject.txtText).getText();
			String ExpectedText = "closed";
			Assert.assertEquals(ExpectedText, VerifyText);
			// delete issue after close
			Functions.commonDelete();
		}

	}

	@AfterMethod
	public void tearDown() throws Exception {
		// logout page
		Functions.commonLogout();
		// close page
		Utils.driver.quit();
	}

}
